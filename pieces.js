module.exports = {
  Pawn, Rook, Bishop, Knight, Queen, King
}

function isValidMove (move, rules) {
  return rules.findIndex((rule) => {
    return rule[0] === move[0] && rule[1] === move[1]
  }) > -1
}

// TODO Add the enpassant move rule
function Pawn(opts) {
  this.color = opts.color
  this.fen = opts.fen

  let hasMoved = false
  let moveRules = [[0,1], [1,0], [1,1], [2,0]]

  this.moveTo = function(move) {
    if (isValidMove(move, moveRules)) {
      hasMoved ? null : moveRules.pop()
      hasMoved = hasMoved || true
      return true
    }
  }

  this.capture = function() {

  }
}

function Rook(opts) {
  this.color = opts.color
  this.fen = opts.fen

  const moveRules = [[0,1], [1,0]]

  this.moveTo = function(move) {
    return isValidMove(move.map(i => i ? i/i : 0), moveRules)
  }
}

function Bishop (opts) {
  this.color = opts.color
  this.fen = opts.fen

  this.moveTo = function (move) {
    try {
      return Math.abs(move[0]/move[1]) === 1
    } catch (e) {
      return false
    }
  }
}

function Knight(opts) {
  this.color = opts.color
  this.fen = opts.fen

  const moveRules = [[2,1],[1,2]]

  this.moveTo = function(move) {
    return isValidMove(move.map(i => Math.abs(i)), moveRules)
  }
}

function Queen(opts) {
  this.color = opts.color
  this.fen = opts.fen

  const moveRules = [[0,1],[1,0]]

  this.moveTo = function(move) {
    if (Math.abs(move[0]) === Math.abs(move[1])) {
      return true
    }
    return isValidMove(move.map(i => i ? i/i : 0), moveRules)
  }
}

function King(opts) {
  this.color = opts.color
  this.fen = opts.fen

  const moveRules = [[0,1], [1,0], [1,1]]

  this.moveTo = function(move) {
    return isValidMove(move.map(i => Math.abs(i)), moveRules)
  }
}
