const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const path = require('path')
const Chess = require('./chess')
const port = 3000

server.listen(port, function () {
  console.log('Server listening at port %d', port)
})

// Holds current game
var game;

// Create route to static files for web chessboard
app.use(express.static(path.join(__dirname, 'public')))

var users = []

io.on('connection', function (socket) {
  // Only allow 2 clients to connect at the same time
  if (users.length < 2) {
    users.push(socket.username)
  }

  if (users.length === 2) {
    game = new Chessboard()
    game.onMove((move) => {
      socket.emit('next move', move)
    })
  }

  socket.on('new game', () => {})

  socket.on('next move', () => {
    // game.next()
  })

  socket.on('disconnect', () => {
    users = []
  })
})
