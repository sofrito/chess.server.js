const { Pawn, Rook, Bishop, Knight, Queen, King } = require('./pieces')

const rankLabels = {
  '1': 0, '2': 1, '3': 2, '4': 3, '5': 4, '6': 5, '7': 6, '8': 7
}

const fileLabels = {
  'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7
}

const pieces = {
  P: Pawn,
  R: Rook,
  B: Bishop,
  N: Knight,
  Q: Queen,
  K: King
}

const MALFORMED_FEN_ERROR = new Error("Malformed FEN")

class Chessboard {
  constructor (fen) {
    this.fen = fen || "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    // Whose turn now: 'w' white | 'b' black
    this.playColor = "w"
    this.kings = []
    const [positions, turnOn, castling, enpassant, halfmoves, fullmoves] = this.fen.split(' ')
    const ranksFen = positions.split('/').reverse()

    this.loadData(ranksFen)
  }

  loadData (ranksFen) {
    // Populate positions matrix holding a Piece object
    // or null on each position
    this.data = ranksFen.map((r, i) => {
      // This is what this map will return
      const result = []

      // Convert the FEN rank string into an array
      const rank = Array.from(r)
      // Parse each character in the rank array
      rank.forEach(c => {
        // If the character is a number then it means
        // a continous number of empty board positions
        const n = parseInt(c)
        // The max amount of empty positions is 8,
        // the min is of course 1
        if (Number.isInteger(n)) {
          if (n > 8 || n < 1) { throw(MALFORMED_FEN_ERROR) }
          for (let i = 0; i < n; i++) { result.push(null) }
          return
        }
        // Otherwise there's a piece in this position,
        // push a new instance of the corresponding piece
        // into the result
        const Piece = pieces[c.toUpperCase()]
        if (!Piece) { throw(MALFORMED_FEN_ERROR) }
        const color = c === c.toUpperCase() ? 'w' : 'b'
        const fen = c
        const piece = new Piece({ color, fen })

        const j = result.length
        // If it's a king, keep track of it
        if (c.match(/k/i)) {
          this.updateKing(piece, [i, j])
        }

        result.push(piece)
      })

      return result
    })
  }

  /**
   * Updates the `kings`
   *
   * @param {Piece} piece The King Piece
   * @return
   * @api public
   */
  updateKing (piece, coords) {
    this.kings[piece.color] = { piece, coords, isChecked: false }
  }

  /**
   * For both kings check if they are 'checked'
   * in the current state
   *
   * @param
   * @return
   */
  checkKingsChecked () {
    const dirs = [[0,1],[1,1],[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1]]

    // Map the pieces that produce a checked
    // on each direction
    const checkedByDir = [
      ['k', 'q', 'r'],
      ['k', 'q', 'b', 'p'],
      ['k', 'q', 'r'],
      ['k', 'q', 'b', 'p'],
      ['k', 'q', 'r'],
      ['k', 'q', 'b', 'p'],
      ['k', 'q', 'r'],
      ['k', 'q', 'b', 'p']
    ]

    Object.keys(this.kings).forEach(color => {
      const king = this.kings[color]
      const kingPos = king.coords

      const checkedDirs = dirs.map((dir, dirIdx) => {
        for (let i = 1; i < 8; i++) {
          const checkRank = dir[0] && kingPos[0] + dir[0] * i
          const checkFile = dir[1] && kingPos[1] + dir[1] * i

          if (
            (checkRank > 7 || checkRank < 0) ||
            (checkFile > 7 || checkFile < 0)
          ) { return }

          const checkPos = [checkRank, checkFile]

          const piece = this.getPieceAt(checkPos)
          if (!piece) { return }

          if (piece.color === color) { return }

          const fen = piece.fen.toLowerCase()
          const pieceCanCheck = checkedByDir[dirIdx].indexOf(fen) > -1
          return pieceCanCheck
        }
      })

      king.checkedDirs = checkedDirs
      king.isChecked = checkedDirs.indexOf(true) > -1
    })
  }

  toggleTurn () {
    return ["w", "b"].filter(t => t !== this.turnOn).pop()
  }

  getPieceAt (coord) {
    const [ rank, file ] = coord
    return this.data[rank][file]
  }

  /**
   * Performs next move
   *
   * @param {String} move The move in Smith notation e.g. d2d4
   * @return
   */
  next (move) {
    const UNKNOWN_MOVE = "Not valid move. What?!"
    const WRONG_COLOR_MOVE = "Not valid move. That's not your piece."
    const NOT_VALID_MOVE = "Not valid move. There is another piece in this position"
    const INVALID_POSITION_MOVE = "Not valid move. This piece can't move there"
    const KING_WILL_CHECKED_MOVE = "Not valid move. Your king gets checked with this move"
    const KING_IS_CHECKED_MOVE = "Not valid move. Your king is checked now"
    const NO_PIECE_IN_POSITION_MOVE = "Not valid move. There's not piece at the origin position"

    const moveMatch = move.match(/([a-h][1-8])([a-h][1-8])/)

    // Unrecognized move notation.
    // This board only knows Smith notation so far.
    if (!moveMatch) {
      return UNKNOWN_MOVE
    }

    // moveMatch[1] and [2] correspond to the origin
    // and target positions in Smith notation (e.g d2)
    const moveFrom = moveMatch[1]
    const moveTo = moveMatch[2]

    // The state matrix coordinates of the origin position
    const moveFromRank = rankLabels[moveFrom[1]]
    const moveFromFile = fileLabels[moveFrom[0]]
    // The state matrix coordinates of the target position
    const moveToRank = rankLabels[moveTo[1]]
    const moveToFile = fileLabels[moveTo[0]]

    // Distance moved in the rank direction
    const rankMoved = moveToRank - moveFromRank
    // Distance moved in the file direction
    const fileMoved = moveToFile - moveFromFile

    // Piece at origin
    const pieceAtOrigin = this.data[moveFromRank][moveFromFile]
    if(!pieceAtOrigin) {
      return NO_PIECE_IN_POSITION_MOVE
    }

    // Piece at target (if any)
    const pieceAtTarget = this.data[moveToRank][moveToFile]

    // Check if the king in turn is checked
    if (this.kings[this.playColor].isChecked) {
      return KING_IS_CHECKED_MOVE
    }

    // Check whether this is a valid move for the piece
    // in the origin position
    const pieceCanMove = pieceAtOrigin.moveTo([rankMoved, fileMoved])

    if (!pieceCanMove) {
      return INVALID_POSITION_MOVE
    }

    // Piece is free to move to target position
    if (!pieceAtTarget) {
      this.movePiece([moveFromRank, moveFromFile], [moveToRank, moveToFile])
      return
    }

    // Otherwise there's a piece on the target position
    // If it's an adversarial piece, capture piece
    if (pieceAtTarget.color !== pieceAtOrigin.color) {
      // If it's a Pawn check that it moved in the
      // its capture direction
      if (pieceAtOrigin.fen.match(/p/i)) {
        return fileMoved === 0 && INVALID_POSITION_MOVE
      }

      if (pieceAtOrigin.capture(pieceAtTarget)) {
        this.removePiece(moveToRank, moveToFile)
        this.movePiece([moveFromRank, moveFromFile], [moveToRank, moveToFile])
      }

      return
    }

    // It it's a friend piece, then can't move there
    return NOT_VALID_MOVE
  }

  movePiece (moveFrom, moveTo) {
    const pieceToMove = this.data[moveFrom[0]][moveFrom[1]]
    this.data[moveFrom[0]][moveFrom[1]] = null
    this.data[moveTo[0]][moveTo[1]] = pieceToMove

    // Check kings for checked after this move
    this.checkKingsChecked()

    // Switch to next player
    this.toggleTurn()
  }

  removePiece (at) {
    this.data[at[0]][at[1]] = null
  }

  render() {
    console.log(this.data)
  }

  toFEN() {
    const state = [...this.data].reverse()
    const positions = state.reduce((fen, rank) => {
      return fen + '/' + rank.map(piece => {
        if (!piece) { return '1' }
        return piece.fen
      }).join("")
    }, "").replace(/(1+)/g, (m, p1) => p1.length)

    return positions + " w KQkq - 0 1"
  }
}

module.exports = Chessboard
