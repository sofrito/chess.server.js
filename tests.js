const Chess = require('./chess')

// Some manual tests that will become automated tests
const game = new Chess()

// Test Pawn
const p1 = game.next("d2d4")
p1 && console.log(p1)

const p2 = game.next("d4d5")
p2 && console.log(p2)

const p3 = game.next("d5d6")
p3 && console.log(p3)

const p4 = game.next("d6d7")
p4 && console.log(p4) // Should not move

// Test Knight
const n1 = game.next("b1a3")
n1 && console.log(n1)

const n2 = game.next("b1a3")
n2 && console.log(n2)

const n3 = game.next("b1a3")
n3 && console.log(n3)


// Render
game.render()

// Dump current game state to FEN
const fen = game.toFEN()
console.log(fen)
